# Desafio - Desenvolvedor Full Stack Junior

## Objetivo:

Desenvolver uma aplicação web full stack para uma plataforma de streaming de vídeos, permitindo aos usuários assistir, favoritar e comentar em vídeos. A aplicação deve ser construída utilizando React ou Angular no frontend e Node.js ou PHP (com Laravel) no backend, com persistência de dados em um banco de dados. ( Necessário ser feito o upload dos videos )

## Requisitos:

## Frontend (React ou Angular):

Criar uma interface de usuário responsiva e intuitiva, utilizando o framework escolhido (React ou Angular) e bibliotecas adicionais conforme necessário para a construção de uma experiência de streaming de vídeos amigável.
Implementar funcionalidades para busca, visualização, reprodução, favoritação e comentário de vídeos.
Desenvolver páginas separadas para exibir categorias de vídeos, permitindo aos usuários explorar conteúdo com base em seus interesses.
Integrar autenticação de usuários, permitindo o registro e login de contas de usuário.
Utilizar chamadas de API para se comunicar com o backend e manipular dados de vídeos e usuários de forma assíncrona.

## Backend:

Criar uma API RESTful utilizando Node.js ou PHP e Laravel para lidar com as operações CRUD (Create, Read, Update, Delete) de vídeos e usuários.
Implementar endpoints para autenticação de usuários, registro e login.
Desenvolver serviços para manipular as operações de banco de dados, incluindo a criação, leitura, atualização e exclusão de vídeos e usuários, utilizando um ORM (Object-Relational Mapping) como Sequelize (Node.js) ou Eloquent (Laravel) para interagir com o banco de dados.

## Banco de Dados:

Configurar um banco de dados para armazenar informações sobre vídeos e usuários.
Definir modelos de dados utilizando o Sequelize ORM (Node.js) ou Eloquent ORM (Laravel) para representar entidades de vídeo e usuário.
Testes:
Implementar testes automatizados para os componentes do frontend (React ou Angular), incluindo testes de unidade para lógica de negócios e testes de integração para simular interações do usuário.
Desenvolver testes de integração para os endpoints da API, cobrindo casos de uso de sucesso e cenários de erro.

## Otimização de Desempenho

- Implementar uma estratégia básica de cache para reduzir a sobrecarga no servidor durante o streaming de vídeos.
- Comprimir os vídeos antes de armazená-los, se possível, para reduzir o tamanho do arquivo e melhorar a velocidade de transferência.

## Diferenciais

- Publicação no Vercel.app
- Uso de Containers Docker
- Build para produção

## Dicas:

Utilize conceitos básicos do framework escolhido (React ou Angular) para criar uma aplicação simples e funcional.
Faça uso de tutoriais e documentação oficial para aprender sobre as funcionalidades e melhores práticas do framework escolhido.
Mantenha o código limpo e bem organizado, seguindo princípios básicos de design de software.
Desenvolva a solução de acordo com os requisitos especificados, garantindo uma plataforma de streaming de vídeos funcional e de qualidade.

## Como Contribuir

- Faça um fork deste repositório.
- Crie uma branch com a sua feature: git checkout -b feature/nova-feature
- Faça commit das suas mudanças: git commit -m 'Adiciona nova feature'
- Faça push para a sua branch: git push origin feature/nova-feature
- Faça um pull request neste repositório.

# Boa Sorte!!
